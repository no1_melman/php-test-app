<?php
	
	require_once( dirname(dirname(__FILE__)) . '/service/admin/admin.service.php');
	require_once( dirname(dirname(__FILE__)) . '/service/page_builder.service.php');
	
	$admin_service = new AdminService();
	$page_builder_service = new PageBuilderService();
	
	$admin_service->redirectToLogin();
	
	$user = $admin_service->getUser();
	
	echo $page_builder_service->getHeader();
?>

	<h1 class="page-header"><?php echo $user->firstname; ?> Profile</h1>

	<div>
		<p>Username: <?php echo $user->username; ?></p>
		<p>First name: <?php echo $user->firstname; ?></p>
		<p>Last name: <?php echo $user->lastname; ?></p>
	</div>

<?php
	echo $page_builder_service->getFooter();
?>