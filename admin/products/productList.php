<?php
    $root = dirname(dirname(dirname(__FILE__)));

	require_once($root . '/service/admin/admin.service.php');
	require_once($root . '/service/page_builder.service.php');
    require_once($root . '/service/routing/url_routing.service.php');
    require_once($root . '/service/product/product.service.php');

	$page_builder_service = new PageBuilderService();
	$url_routing = new UrlRouting();
    $admin_service = new AdminService();

    $admin_service->redirectToLogin();

    $error = "";
    $errorOccured = FALSE;

    try{
        $product_service = new ProductService();
        $products = $product_service->getAll();
    }catch(Exception $e){
        $error = $e->getMessage();
        $errorOccured = TRUE;
    }

    if($product_service->error){
        $error = $product_service->message;
        $errorOccured = TRUE;
    }

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">All Products</h1>

<button data-toggle="modal" data-target="#createProduct" class="btn btn-primary bt-lg">Create Product</button>

<div data-bind="visible: showMsg, attr { class: error() ? 'alert alert-danger' : 'alert alert-success' }">
	<p data-bind="text: message"></p>
</div>
	
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody data-bind="foreach: products">
		<tr>
			<td data-bind="text: name"></td>
			<td data-bind="text: description"></td>
			<td data-bind="text: price"></td>
		</tr>
	</tbody>
</table>

<div class="modal fade" id="createProduct" tabindex="-1" role="dialog" aria-labelledby="CreateProduct" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Create Product</h4>
      </div>
      <div class="modal-body">
		<div class="form-horizontal">
        <h4>Product</h4>
			<div class="form-group">
				<label class="col-sm-3 control-label">Name:</label>
				<div class="col-sm-9">
					<input type="text" id="name" name="name" class="form-control" placeholder="Name" data-bind="value: name"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Description:</label>
				<div class="col-sm-9">
					<textarea id="description" name="description" class="form-control" placeholder="Description" rows="6" data-bind="value: desc">

					</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Price:</label>
				<div class="col-sm-9">
					<div class="input-group">
					  <span class="input-group-addon">&pound;</span>
					  <input type="text" class="form-control" id="price" name="price" data-bind="value: price">
					</div>
				</div>
			</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-bind="click: saveChanges()" data-dismiss="modal">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
    echo $page_builder_service->getScripts();
?>

<script type="text/javascript">
	var productViewModel = function () {
		var self = this;
		
		self.name = ko.observable("");
		self.desc = ko.observable("");
		self.price = ko.observable("");
		
		self.products = ko.observableArray([]);
		
		self.saveChanges = function () {
			var request = $.ajax({
				url: "<?php echo $url_routing->ParseUrl('service/ajax/products/product_create.ajax.service.php'); ?>",
				dataType: "json",
				type: "POST",
				data: { name: self.name(), desc: self.desc(), price: self.price() }
			});
			
			request.done(function (data, result) {
				if (result == "success") {
					if (data.success) {
						self.error(false);
						self.message("Added product successfully");
					}else{
						self.error(true);
						self.message("Server Error: Unable to add product");
					}
				}else{
					self.error(true);
					self.message("Ajax Error");
				}
				
				self.showMsg(true);
			});
		};
		
		self.getProducts = function () {
			 var request = $.ajax({
				url: "<?php echo $url_routing->ParseUrl('service/ajax/products/product_list.ajax.service.php'); ?>",
				type: "GET",
				dataType: "json"
			 });
			 
			 request.done(function (data, result) {
				if (result == "success") {
					self.products(data);
				}
			 });
		};
		
		self.showMsg = ko.observable(false);
		self.error = ko.observable(false);
		self.message = ko.observable("");
		
		self.getProducts();
	};
	
	ko.applyBindings(new productViewModel());
</script>

<?php
echo $page_builder_service->getFooter(FALSE);
?>