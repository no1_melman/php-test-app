<?php
    $root = dirname(dirname(dirname(__FILE__)));

	require_once( $root . '/service/admin/admin.service.php');
	require_once( $root . '/service/page_builder.service.php');
    require_once($root . '/service/routing/url_routing.service.php');

    $admin_service = new AdminService();
	$page_builder_service = new PageBuilderService();
    $url_routing = new UrlRouting();

    $admin_service->redirectToLogin();

    $error = isset($_GET['error']);

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">Add Product</h1>

<?php
	if ($error) {
?>
	<div class="alert alert-danger">
		<p>
			There was an error processing your request.
		</p>
	</div>
<?php } ?>

<form action="<?php echo $url_routing->ParseUrl('service/forms/product/product.add.form.php') ?>" method="POST" class="form-horizontal login-form">
	<h4>Product</h4>
	<div class="form-group">
		<label class="col-sm-3 control-label">Name:</label>
		<div class="col-sm-9">
			<input type="text" id="name" name="name" class="form-control" placeholder="Name"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Description:</label>
		<div class="col-sm-9">
		    <textarea id="description" name="description" class="form-control" placeholder="Description" rows="6">

		    </textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Price:</label>
		<div class="col-sm-9">
			<div class="input-group">
              <span class="input-group-addon">&pound;</span>
              <input type="text" class="form-control" id="price" name="price">
            </div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
		</div>
	</div>
</form>


<?php
echo $page_builder_service->getFooter();
?>