<?php

    $root = dirname(dirname(dirname(__FILE__)));

    require_once($root . '/service/admin/admin.service.php');

    $admin_service = new AdminService();

    $admin_service->logOut();

    header("Location: ../../home.php");
    exit;

?>