function tableBuilder(headers, content) {
    var table = document.createElement('table');
    table.setAttribute('class','table table-striped table-hover');
    var thead = document.createElement('thead');
    
    var theadTr = document.createElement('tr');
    
    for (var i = 0; i < headers.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = headers[i];
        theadTr.appendChild(th);
    }
    
    thead.appendChild(theadTr);
        
    var tbody = document.createElement('tbody');
        
    content = content instanceof Array ? content : [content]; // always makes sure content is an array

    
    for (var i = 0; i < content.length; i++) {
        var item = content[i];
        console.log(item);
        var tr = document.createElement('tr');
        
        for (var value in item) {
         var td = document.createElement('td');
            td.innerHTML = item[value];
            tr.appendChild(td);
        }
        
        var td = document.createElement('td');
        var button = document.createElement('a');
        button.setAttribute('class', 'btn btn-info btn-sm');
        button.setAttribute('onclick', 'addToCart(' + i +')');
        var glypSpan = document.createElement('span');
        glypSpan.setAttribute('class', 'glyphicon glyphicon-shopping-cart');
        button.appendChild(glypSpan);
        td.appendChild(button);
        tr.appendChild(td);
        
        tbody.appendChild(tr);
    }
    
    table.appendChild(thead);
    table.appendChild(tbody);
    
    return table;
}