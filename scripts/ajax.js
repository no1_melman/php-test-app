function ajax(options, callback) {
    var xmlhttp;

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            callback(xmlhttp.responseText); 
        }
    }
    
    if (options.data && options.method == "GET") {
        var appendToUrl = "?";
        for (var dataItem in options.data) {
            appendToUrl += dataItem + "=" + options.data[dataItem] + "&";
        }
        options.url += appendToUrl;
    }

    xmlhttp.open(options.method, options.url, true);
    xmlhttp.send();
}