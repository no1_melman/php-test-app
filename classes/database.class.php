<?php

class Database {
    
	private $pdoConnection;
	
	private $db_host = "localhost";
	private $db_name = "my_database";
	private $db_user = "root";
	private $db_pass = "";
	
	private $db_connection = "";
	
	public $db_connected = FALSE;
	
	private $_errorList = array();
	
	public function __construct() {
		$this->db_connection = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name;
		
		try
		{
			$this->pdoConnection = new PDO($this->db_connection, $this->db_user, $this->db_pass);
			$this->db_connected = TRUE;
		}catch(PDOException $e){
			array_push($this->_errorList, $e->getMessage());
		}
	}
	
	public function init() {
		try{
			$this->pdoConnection = new PDO('mysql:host=' . $this->db_host . ';', $this->db_user, $this->db_pass);

			$query = $this->pdoConnection->query("CREATE DATABASE IF NOT EXISTS " . $this->db_name);
			
			$query = $this->pdoConnection->query("SHOW DATABASES LIKE '$this->db_name'");
			
			$results = $query->fetchAll();
			
			if ($results[0]) {
				$this->pdoConnection = null;
				$this->pdoConnection = new PDO($this->db_connection, $this->db_user, $this->db_pass);
				$this->db_connected = TRUE;
				return TRUE;
			}else{
				throw new Exception("Database couldn\'t be created");
			}
		}catch(Exception $e) {
			$this->db_connected = FALSE;
			array_push($this->_errorList, $e->getMessage());
		}
		
		return FALSE;
	}
	
	public function queryPrepared($query) {
		return $this->pdoConnection->prepare($query);
	}
	
	public function queryUnprepared($query) {
		return $this->pdoConnection->query($query);
	}
	
	public function connected(){
		return $this->db_connected;
	}
	
	public function getErrors(){
		if (count($this->_errorList) < 1) {
			return null;
		}
			
		return $this->_errorList;
	}
	
	public function clearErrors() {
		$this->_errorList = array();
	}

	public function __destruct() {
		$pdoConnection = null;
	}
}

?>