<?php
	
	require_once 'service/admin/admin.service.php';
	require_once 'service/page_builder.service.php';

	$error = isset($_GET['fail']);
	
	$page_builder_service = new PageBuilderService();

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">Register</h1>

<?php
	if ($error) {
?>
	<div class="alert alert-danger">
		<p>
			There was an error processing your request.
		</p>
	</div>
<?php } ?>

<form action="service/register.service.php" method="POST" class="form-horizontal login-form">
	<h4>Account</h4>
	<div class="form-group">
		<label class="col-sm-3 control-label">Username:</label>
		<div class="col-sm-9">
			<input type="text" id="username" name="username" class="form-control" placeholder="Username"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Password:</label>
		<div class="col-sm-9">
			<input type="password" id="password" name="password" class="form-control" placeholder="Password"/>
		</div>
	</div>
	<h4>Profile</h4>
	<div class="form-group">
		<label class="col-sm-3 control-label">First Name:</label>
		<div class="col-sm-9">
			<input type="text" id="fname" name="fname" class="form-control" placeholder="First Name"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Last Name:</label>
		<div class="col-sm-9">
			<input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
		</div>
	</div>
</form>

<?php
echo $page_builder_service->getFooter(); 
?>