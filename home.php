<?php
	$root = dirname(__FILE__);

	require_once($root . '/service/product/product.service.php');
	require_once($root . '/service/page_builder.service.php');
	
	$page_builder_service = new PageBuilderService();
	
	$error = "";
	$errorOccured = FALSE;
	
	try{
		$product_service = new ProductService();
		$products = $product_service->getAll();
        
	}catch(Exception $e){
		$error = $e->getMessage();
		$errorOccured = TRUE;
	}
	
	if($product_service->error){
		$error = $product_service->message;
		$errorOccured = TRUE;
	}

	echo $page_builder_service->getHeader();
?>
	
	<h1 class="page-header">Home</h1>
	
	<?php 
		if (isset($_GET['init'])) {
	?>
		<div class="alert alert-warning">
			<p>Database initialised for first time use</p>
		</div>
	<?php } ?>
	
	<h3>Products</h3>

    <div id="jsonTable">

    </div>

<!-- BOTTOM OF THE PAGE -->
<?php echo $page_builder_service->getScripts(); ?>

<script type="application/javascript">
    var products;
    var fireMethod = false;

    ajax({
        method: "GET",
        url: "/TestApp_Latest/service/ajax/products/product_list.ajax.service.php"
    }, function (response) {
        if (response) {

            var insert = document.getElementById('jsonTable');
            products = JSON.parse(response);
            var table = tableBuilder(["Id","Name","Description","Price", ""], products);
            insert.appendChild(table);

            fireMethod = true;
        }
    });
    
    function addToCart (jsonIndex) {
        if (!fireMethod) return;

        var product = products[jsonIndex];

        ajax({
            method: "GET",
            url: "/TestApp_Latest/service/ajax/basket/basket_add.ajax.service.php",
            data: { id: product.id, name: product.name, description: product.description, price: product.price, qty: 1  }
        }, function (response) {
            if (response) {
                var obj = JSON.parse(response);
                console.log(obj);
                if (obj.success) {
                    var basket = document.getElementById("items");
                    basket.innerHTML = parseInt(basket.innerHTML, 10) + 1;
                }
            }
        });
    }
</script>

<?php echo $page_builder_service->getFooter(FALSE); ?>
