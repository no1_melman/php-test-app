<?php

class Product {
	public $id;
	public $name;
	public $description;
	public $price;
}

class LineItem {
    public $product;
    public $qty;
    public $total;

    public function __construct() {
        $this->product = new Product();
    }

}

class User {
	public $id;
	public $username;
	public $firstname;
	public $lastname;
	public $password;
}

class SessionUser {
	public $username;
	public $firstname;
	public $lastname;
}

?>