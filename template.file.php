<?php
	$root = dirname(dirname(dirname(__FILE__)));

    require_once( $root . '/service/admin/admin.service.php');
    require_once( $root . '/service/page_builder.service.php');

    $admin_service = new AdminService();
    $page_builder_service = new PageBuilderService();

    $admin_service->redirectToLogin();
	$page_builder_service = new PageBuilderService();

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">Template Heading</h1>



<?php
echo $page_builder_service->getFooter(); 
?>