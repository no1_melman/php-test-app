<?php
	function exception_handler($exception) {
		$_GLOBAL['HIT_ERROR'] = $exception->getMessage();
		header('Location: error.php');
		exit;
	}

	set_exception_handler('exception_handler');

	require_once(dirname(__FILE__) . '/classes/exceptions.class.php');
	require_once(dirname(__FILE__) . '/service/init.service.php');
?>