<?php
	$root = dirname(__FILE__);
	require_once($root . '/service/admin/admin.service.php');
	require_once($root . '/service/page_builder.service.php');

	$page_builder_service = new PageBuilderService();

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">Log In</h1>

<?php
    if (isset($_GET['fail'])) {
?>
<div class="alert alert-danger">
    <p>There was an error processing your request.</p>
    <p>Your username or password may be incorrect.</p>
</div>
<?php
    }
?>

<form action="service/login.service.php" method="POST" class="form-horizontal login-form">
	<div class="form-group">
		<label class="col-sm-2 control-label">Username:</label>
		<div class="col-sm-10">
			<input type="text" id="username" name="username" class="form-control" placeholder="Username"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Password:</label>
		<div class="col-sm-10">
			<input type="password" id="password" name="password" class="form-control" placeholder="Password"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
		</div>
	</div>
</form>

<?php
echo $page_builder_service->getFooter(); 
?>