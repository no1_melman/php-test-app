<?php
	$root = dirname(__FILE__);

    require_once( $root . '/service/admin/admin.service.php');
    require_once( $root . '/service/page_builder.service.php');
    require_once( $root . '/service/basket/basket.service.php');

    $admin_service = new AdminService();
    $page_builder_service = new PageBuilderService();
    $basket_service = new BasketService();

    $admin_service->redirectToLogin();

    $basket = $basket_service->lineItems();

	echo $page_builder_service->getHeader();
?>

<h1 class="page-header">Basket</h1>


<?php
    if ($basket_service->hasLineItems()) {
        ?>

        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($basket as $lineItem) {
                        echo '<tr><td>' . $lineItem->product->name . '</td><td>' . $lineItem->product->description . '</td><td>' . $lineItem->product->price . '</td><td>' . $lineItem->qty . '</td><td>' . $basket_service->totalPrice($lineItem->qty, $lineItem->product->price) . '</td></tr>';
                    }
                ?>
            </tbody>
        </table>

<?php } ?>

<?php
echo $page_builder_service->getFooter();
?>