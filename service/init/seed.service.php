<?php

class Seed {
	private $context;
	
	public function __construct(){
		
	}
	
	public function seed($context) {
		$this->context = $context;
		
		if (!$this->context->connected()) {
			throw new SeedException('Database not connected. Can\'t seed database.', 1);
		}
		
		try{
			$seedData = file_get_contents('tables.sql');
			
			$result = $this->context->queryUnprepared($seedData);
		}catch(Exception $e){
			if ($e instanceof PDOException) {
				throw new SeedException('Database error: ' . $e->getMessage(), 2);
			}
			
			throw new SeedException('Unknown error, possibly file_get_contents: ' . $e->getMessage(), 2);
		}
	}
}

?>