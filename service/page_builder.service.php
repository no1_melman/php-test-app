<?php
class PageBuilderService {
	public function getHeader() {
		// return file_get_contents('template/header.html');
		include(dirname(dirname(__FILE__)) . '/template/header.php');
	}
    
    public function getScripts() {
        return file_get_contents(dirname(dirname(__FILE__)) . '/template/scripts.html');   
    }
	
	public function getFooter($includeScripts = true) {
		return $includeScripts ? 
			file_get_contents(dirname(dirname(__FILE__)) . '/template/footer.html') . $this->getScripts() 
			: file_get_contents(dirname(dirname(__FILE__)) . '/template/footer.html');
	}
}
?>