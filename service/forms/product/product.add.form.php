<?php
    $root = dirname(dirname(dirname(dirname(__FILE__))));

	require_once( $root . '/service/admin/admin.service.php' );
    require_once( $root . '/service/routing/url_routing.service.php' );
    require_once( $root . '/service/product/product.service.php' );

    $admin_service = new AdminService();
    $url_routing = new UrlRouting();
    $product_service = new ProductService();

    $admin_service->redirectToLogin();

    $product = new Product();
    $product->name = $_POST['name'];
    $product->description = $_POST['description'];
    $product->price = $_POST['price'];

    $success = $product_service->createProducts($product);

    if ($success) {
        header("Location: ../../../admin/products/productList.php?success=true");
        exit;
    }else{
        header("Location: ../../../admin/products/addProduct.php?error=true");
        exit;
    }

?>