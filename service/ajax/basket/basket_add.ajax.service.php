<?php
    $root = dirname(dirname(dirname(dirname(__FILE__))));

	require_once($root . '/service/basket/basket.service.php');

    $lineItem = new LineItem();

    $lineItem->product->id = $_GET['id'];
    $lineItem->product->name = $_GET['name'];
    $lineItem->product->description = $_GET['description'];
    $lineItem->product->price = $_GET['price'];
    $lineItem->qty = $_GET['qty'];

    $basket_service = new BasketService();

    $error = "";
	$errorOccured = FALSE;

    $json = array();

	try{
		$basket_service->addToBasket($lineItem);
        $json = array( 'success' => TRUE, 'msg' => 'Product added to basket successfully' );
	}catch(Exception $e){
		$error = $e->getMessage();
		$errorOccured = TRUE;
        
        $json = array( 'basket_error' => 'true', 'basket_msg' => $error );
	}

    echo json_encode($json);
?>