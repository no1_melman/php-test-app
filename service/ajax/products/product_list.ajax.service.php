<?php

$root = dirname(dirname(dirname(dirname(__FILE__))));
require_once ($root . '/service/product/product.service.php');

$product_service = new ProductService();

$products = $product_service->getAll();

$jsonProducts = array();

foreach ($products as $product) {
    array_push($jsonProducts, $product_service->productToJson($product));
}

echo json_encode($jsonProducts);

?>