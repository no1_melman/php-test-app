<?php

$root = dirname(dirname(dirname(dirname(__FILE__))));
require_once ($root . '/service/product/product.service.php');

$product_service = new ProductService();

$product = new Product();

$product->name = $_POST['name'];
$product->description = $_POST['desc'];
$product->price = $_POST['price'];

$jsonResponse = array('success' => false);

if ($product->name == "" || $product->description == "" || $product->price == "") {
	$jsonResponse['success'] = false;
} else if ($product_service->createProduct($product)) {
    $jsonResponse['success'] = true;
}

echo json_encode($jsonResponse);

?>