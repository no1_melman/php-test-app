<?php
    require_once(dirname(dirname(__FILE__)) . '/service/user/user.service.php');
	require_once(dirname(dirname(__FILE__)) . '/service/admin/admin.service.php');

	$user_service = new UserService();
    $user = new User();

    $user->username = $_POST['username'];
    $user->password = $_POST['password'];

    $db_user = $user_service->loginUser($user);

    if (!empty($db_user)){
        $admin_service = new AdminService();
        $sessionUser = $user_service->toSessionUser($db_user);
        $admin_service->logInUser($sessionUser);
        header("Location: /TestApp/admin/profile.php");
        exit;
    }else{
        header("Location: /TestApp/login.php?fail=true");
        exit;
    }

?>