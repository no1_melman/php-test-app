<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/models/all.models.php');

class AdminService {
	public function __construct() {
		if(session_id() == '' || !isset($_SESSION)) {
            // session isn't started
            session_start();
        }
	}
	
	public function logInUser($user) {		
		$this->setUser($user);
	}
	
	public function logOut() {
		unset($_SESSION['user']);
		session_destroy();
	}
	
	private function setUser($user) {
		$_SESSION['user'] = serialize($user);
	}
	
	public function getUser() {
		return unserialize($_SESSION['user']);
	}

	public function loggedIn() {
		return isset($_SESSION['user']);
	}

	public function redirectToLogin() {
	    if (!$this->loggedIn()) {
            header("Location: /TestApp/login.php");
            exit;
	    }
	}
	
	public function __destruct() {
	}
}

?>