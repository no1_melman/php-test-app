<?php
$root = dirname(dirname(dirname(__FILE__)));
require_once( $root  . "/classes/database.class.php");
require_once( $root . "/models/all.models.php");

class UserService {
	private $database;
	
	public $error = FALSE;
	public $message = "";
	
	public function __construct() {
		$this->database = new Database();
	}

	public function createUser($user) {
		$stmt = $this->database->queryPrepared("INSERT INTO users(firstname, lastname, username, password) VALUES (:fname, :lname, :uname, :pword)");
		
		$stmt->bindParam(":fname", $fname);
		$stmt->bindParam(":lname", $lname);
		$stmt->bindParam(":uname", $uname);
		$stmt->bindParam(":pword", $pword);
		
		$fname = $user->firstname;
		$lname = $user->lastname;
		$uname = $user->username;
		$pword = $this->encryptPassword($user->password);

		$result = $stmt->execute();

		return $result;
	}
	
	private function encryptPassword($password) {
		return hash('sha256', $password);
	}
	
	private function getUser($uname) {
		$stmt = $this->database->queryPrepared("SELECT * FROM users WHERE username=:uname");

		$stmt->bindParam(":uname", $b_uname);
		$b_uname = $uname;
		
		$result = $stmt->execute();

        $user = null;

		if($result) {
			$users = $stmt->fetchAll(PDO::FETCH_CLASS, "User");
			if (count($users) > 0) {
				$user = $users[0];
			}else{
				$user = new User();
			}
        }
		
		return $user;
	}

	public function loginUser($user) {
	    $db_user = $this->getUser($user->username);

	    $success = null;

	    if ($db_user->password == $this->encryptPassword($user->password)){
	        $success = $db_user;
	    }

	    return $success;
	}

	public function updateUser($user) {
		$stmt = $this->database->queryPrepared("UPDATE users SET firstname=:fname, lastname=:lname WHERE username=:uname");

		$stmt->bindParam(":fname", $fname);
		$stmt->bindParam(":lname", $lname);
		$stmt->bindParam(":uname", $uname);
		
		$uname = $user->firstname;
		$uname = $user->lastname;
		
		$return = $stmt->execute();
		
		return $return;
	}
	
	public function changePassword($oldPassword, $newPassword, $uname) {
		$user = $this->getUser($uname);
		
		$sha1OldPassword = $this->encryptPassword($oldPassword);
		
		if ($sha1OldPassword == $user->password) {
			$user->password = $this->encryptPassword($newPassword);
			
			return $this->updatePassword($user);
		} else {
			return false;
		}
	}
	
	private function updatePassword($user) {
		$stmt = $this->database->queryPrepared("UPDATE users SET password=:pword WHERE username=:uname");

		$stmt->bindParam(":pword", $pword);
		$stmt->bindParam(":uname", $uname);
		
		$pword = $user->password;
		$uname = $user->username;
		
		$return = $stmt->execute();
		
		return $return;
	}

	public function toSessionUser($user) {
	    $sessionUser = new SessionUser();
	    $sessionUser->username = $user->username;
        $sessionUser->firstname = $user->firstname;
        $sessionUser->lastname = $user->lastname;
        return $sessionUser;
	}
	
	public function __destruct() {
		$this->database->__destruct();
	}
}

?>