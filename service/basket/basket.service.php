<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/models/all.models.php');

class BasketService {
	public function __construct() {
		if(session_id() == '' || !isset($_SESSION)) {
            // session isn't started
            session_start();
        }

        if (!isset($_SESSION['basket'])) {
            $this->setBasket(array());
        }
	}

	public function addToBasket($lineItem) {
        $lineItems = $this->getBasket();

        $foundLineItem = $this->findByName($lineItems, $lineItem);

        if (is_null($foundLineItem)) {
            $lineItem->id = $this->generateId($lineItems);
            array_push($lineItems, $lineItem);
            $this->setBasket($lineItems);
        }else{
            $foundLineItem->qty += $lineItem->qty;
            $this->updateBasket($foundLineItem);
        }
   	}

    // usages:
    //     to completely remove an item:
    //         $basket_service->removeFromBasket($lineItem)
    //         $basket_service->removeFromBasket($lineItem, [SOME QTY MORE THAN WHAT IS IN THE LINEITEM]);
    //     to reduce the number of items in a lineItem:
    //         $basket_service->removeFromBasket($lineItem, [SOME QTY LESS THAN WHAT IS IN THE LINEITEM]);
	public function removeFromBasket($lineItem, $qty = 0) {
        // if qty is equal to 0, remove the whole lineitem
        $lineItems = $this->getBasket();

        if ($qty == 0) {
            $this->setBasket($this->removeById($lineItem->id)); // if there is no qty then remove entire lineItem;
        }else{
            $lineItem->qty = $lineItem->qty - $qty; // work out the new qty
            if ($lineItem->qty < 1 ){
                $this->setBasket($this->removeById($lineItem->id)); // if the qty is less than 1 then remove the entire lineItem
            }else{
                $this->updateBasket($lineItem); // just update the qty
            }
        }
	}

	public function updateBasket($lineItem) {
        // this is the lazy mans way of updating the basket
	    $basket = $this->removeById($lineItem->id); // first remove the lineItem altogether

	    array_push($basket, $lineItem); // secondly re-push the lineItem from the form to the basket

	    $this->setBasket($basket); // set the basket again
	}

	public function lineItems($limit = 0, $count = 10) {
	    if ($limit == 0) {
	        return $this->getBasket();
	    } else {
	        return array_slice($this->getBasket(), $count);
	    }
	}

	public function totalPrice($qty, $price) {
	    return $qty * $price;
	}

	public function clearBasket() {
		unset($_SESSION['basket']);
	}

	public function hasBasket() {
		return isset($_SESSION['basket']);
	}

    public function hasLineItems() {
        if ($this->hasBasket()){
            $lineItems = $this->getBasket();
            return count($lineItems) > 0;
        }

        return FALSE;
    }

	// ----------- PRIVATE FUNCTIONS ------------- \\
	private function setBasket($basket) {
        $_SESSION['basket'] = serialize($basket);
    }

    private function getBasket() {
        return unserialize($_SESSION['basket']);
    }

    private function removeById ($lineItemId) {
        $basket = $this->getBasket();

        $newBasket = array();

        foreach($basket as $lineItem) {
           if ($lineItem->id != $lineItemId) {
               array_push($newBasket, $lineItem);
           }
        }

        return $newBasket;
    }

    private function generateId($basket) {
        $usedIds = array(); // create a new array

        foreach($basket as $lineItem) {
          array_push( $usedIds, $lineItem->id ); // populate array with ids from the basket
        }

        $found = FALSE; // we havent found a unique id yet
        $currentId = 0; // set the current id to 0

        while($found == FALSE) { // while we stil havent found a new id
            if (!$this->containsId($usedIds, $currentId)) { // check to see if the current id doesn't exists
                $found = TRUE; //if it doesn't then we have found a unique id
            }else{
                $currentId++; // else increment the id and test again
            }
        }

        return $currentId; // return the new unique current id
    }

    private function containsId($ids, $chosenId) {
        foreach ($ids as $id) {
            if ($id == $chosenId) {
                return true;
            }
        }

        return false;
    }

    private function findByName($lineItems, $lineItem) {
        foreach($lineItems as $item) {
            if ($item->product->name == $lineItem->product->name) {
                return $item;
            }
        }

        return null;
    }

	public function __destruct() {
	}
}

?>