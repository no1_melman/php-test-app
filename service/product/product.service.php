<?php
$root = dirname(dirname(dirname(__FILE__)));

require_once($root . '/classes/database.class.php');
require_once($root . '/models/all.models.php');

class ProductService {
	private $database;
	
	public $error = FALSE;
	public $message = "";
	
	public function __construct() {
		$this->database = new Database();
	}
	
    public function getById($productId) {
        try {
           $stmt = $this->database->queryPrepared('SELECT * FROM products WHERE id=:id LIMIT 1');
            
            $stmt->bindParam(":id", $id);
            $id = $productId;
            
            if ($stmt->execute()) {
                $product = $stmt->fetchAll(PDO::FETCH_CLASS, "Product");
                return $product; 
            }
            
            return null;
            
        }catch(PDOException $e) {
            $this->error = TRUE;
			$this->message = $e->getMessage();
        }
        
        return null;
    }
    
	public function getAll() {
		try {		
			$stmt = $this->database->queryUnprepared('SELECT * FROM products');

			$_products = $stmt->fetchAll(PDO::FETCH_CLASS, "Product");
            
			$stmt->closeCursor();
			
			$this->error = FALSE;
			
			return $_products;
		}catch (PDOException $e){
			$this->error = TRUE;
			$this->message = $e->getMessage();
		}
	}
        
	
	public function getHighestPrice($products = null) {
		$_products = $products == null ? $this->getAll() : $products;
			
		$priceSoFar = 0;
		foreach($_products as $product){
			$priceSoFar = $product->price > $priceSoFar ? $product->price : $priceSoFar;
		}
			
		return $priceSoFar;
	}
	
	public function createProduct($product){
		$stmt = $this->database->queryPrepared("INSERT INTO products(name, description, price) VALUES (:name, :desc, :price)");
		$stmt->bindParam(":name", $name);
		$stmt->bindParam(":desc", $desc);
		$stmt->bindParam(":price", $price);

		$success = false;

        $name = $product->name;
        $desc = $product->description;
        $price = floatval($product->price);

        $success = $stmt->execute();

        var_dump($success);

		return $success;
	}
    
    public function productToJson($product) {
	    return array( 'id' => $product->id, 'name' => $product->name, 'description' => $product->description, 'price' => $product->price );
	}
	
	public function __destruct(){
		$this->database->__destruct();
	}
}

?>