<?php

	require_once(dirname(dirname(__FILE__)) . '/service/user/user.service.php');
	require_once(dirname(dirname(__FILE__)) . '/service/admin/admin.service.php');

	$user_service = new UserService();
	$user = new User();
	$sessionUser = new SessionUser();

	$user->username = $_POST['username'];
	$user->password = $_POST['password'];
	$user->firstname = $_POST['fname'];
	$user->lastname = $_POST['lname'];

    $result = $user_service->createUser($user);

	if ($result) {
		$admin_service = new AdminService();
		$sessionUser->username = $user->username;
		$sessionUser->firstname = $user->firstname;
		$sessionUser->lastname = $user->lastname;
		$admin_service->logInUser($sessionUser);
		header("Location: ../admin/profile.php");
		exit;
	}else{
		header("Location: ../register.php?fail=true");
		exit;
	}

?>