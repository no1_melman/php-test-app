<?php

	require_once( dirname(dirname(__FILE__)) . '/classes/database.class.php');
	require_once( dirname(dirname(__FILE__)) . '/service/init/seed.service.php');
	require_once( dirname(dirname(__FILE__)) . '/classes/exceptions.class.php');


	$had_to_init=FALSE;
	
	$database = new Database();
	
	$errors = $database->getErrors();
		
	if ($errors != null) {
		echo "Database not initialised. <br />";
	}
	
	$database->clearErrors();
	
	if(!$database->connected()){
		$init = $database->init();
		
		if($init){
			try{
				$seed = new Seed();
				$seed->seed($database);
				echo "Database initialised. <br />";
				$had_to_init=TRUE;
			}catch(SeedException $e){
				echo $e->getMessage() . '<br />';
			}
		}else{
			echo "Failed initialisation <br />";
		}
	}

	$database = new Database();
	
	$errors = $database->getErrors();
		
	if ($errors != null) {
		echo "There are errors after initialisation: <br />";
		var_dump($errors);
	}else{
		$location = $had_to_init ? 'home.php?init=true' : 'home.php'; 
		header('Location: ' . $location);
		exit;
	}
?>