<?php
    $root = dirname(dirname(__FILE__));
	require_once($root . '/service/admin/admin.service.php');
	require_once($root . '/service/routing/url_routing.service.php');
	require_once($root . '/service/basket/basket.service.php');

	$admin_service = new AdminService();
	$url_routing = new UrlRouting();
	$basket_service = new BasketService();

	$items = count($basket_service->lineItems());
?>

<html>
<head>
<title>TestApp</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/TestApp_Latest/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="/TestApp_Latest/css/bootstrap-theme.min.css" />

<style>
	.top-margin { margin-top:60px; }
	
	.login-form {
		width:500px !important;
		margin:0 auto;
	}
</style>

</head>	
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TestApp</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $url_routing->ParseUrl('') ?>">Home</a></li>
            <li><a href="#about">About</a></li>
			<li><a href="<?php echo $url_routing->ParseUrl('basket.php') ?>">Basket (<span id="items"><?php echo $items; ?></span>)</a></li>
			<?php 
				if (!$admin_service->loggedIn()) {
				?>
            <li><a href="<?php echo $url_routing->ParseUrl('login.php') ?>">Log In</a></li>
			<li><a href="<?php echo $url_routing->ParseUrl('register.php') ?>">Register</a></li>
			<?php 
				}else{
				?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $url_routing->ParseUrl('admin/profile.php') ?>">Profile</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Account</li>
                <li><a href="<?php echo $url_routing->ParseUrl('admin/account/changePassword.php') ?>">Change Password</a></li>
                <li><a href="<?php echo $url_routing->ParseUrl('admin/account/logout.php') ?>">Log Out</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $url_routing->ParseUrl('admin/products/productList.php') ?>">All Products</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Modify</li>
                <li><a href="<?php echo $url_routing->ParseUrl('admin/products/addProduct.php') ?>">Add Product</a></li>
              </ul>
            </li>
			<?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

	<div class="container top-margin" role="main">
		
		